import requests
from pathlib import Path
import zipfile
import io
import subprocess
import sys


script_dir = Path(__file__).parent.resolve()

download_dir = script_dir / "download"

if not download_dir.exists():
    download_dir.mkdir()

url = "https://rwth-aachen.sciebo.de/s/hYw1oGdFz9tfZVj/download"
r = requests.get(url, allow_redirects=True)

with zipfile.ZipFile(io.BytesIO(r.content)) as zip_ref:
    zip_ref.extractall(download_dir)

va_python_whl_list = list(download_dir.glob("VA*/VAPython*/VAPython*.whl"))

if not va_python_whl_list:
    raise LookupError("No VAPython whl found!")

va_python_whl = va_python_whl_list[0]

subprocess.check_call([sys.executable, "-m", "pip", "install", va_python_whl])
