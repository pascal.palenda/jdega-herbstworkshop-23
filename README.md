# jDEGA Herbstworkshop 2023

These are the Jupyter notebooks used for the jDEGA Herbstworkshop in 2023 at the Institute for Hearing Technology and Acoustics.

## How to run

It is required, that Python 3 is installed on the system.

Run the `setup.bat` script to set up a virtual environment and install all required packages.

When this is done, the `run.bat` script will start Jupyter Lab and automatically open a web browser with the notebooks.
