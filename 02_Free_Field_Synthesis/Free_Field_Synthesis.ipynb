{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Free-field synthesis\n",
    "\n",
    "After we explored the basics of binaural synthesis, we will expand on it by auralizing a free-field environment.\n",
    "A \"free-field\" is defined by the absence of any reflections.\n",
    "As a result, only one single path exists connecting the sound source with us as the receiver.\n",
    "\n",
    "To start, we will again load the HRTF dataset as well as the audio samples.\n",
    "It should be noted here, that for a true free-field auralization, the audio samples must also be recorded under anechoic conditions which can be considered as a free-field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "import pyfar as pf\n",
    "import numpy as np\n",
    "import helper_functions.helper_functions as hf\n",
    "import matplotlib.pyplot as plt\n",
    "from scipy.spatial.transform import Rotation\n",
    "\n",
    "\n",
    "# load required data\n",
    "data_ir, source_coordinates, receiver_coordinates = pf.io.read_sofa(\n",
    "    \"../data/hrir/ITA_Artificial_Head_5x5_44100Hz.sofa\")\n",
    "\n",
    "audio_data_guitar = pf.io.read_audio(\"../data/audio/guitar.wav\")\n",
    "audio_data_horns = pf.io.read_audio(\"../data/audio/horns.wav\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the first step, we use the helper function `get_3D_metrics` to calculate the relative angle between us (the receiver) and the source.\n",
    "This function uses simple 3D vector math and trigonometric functions to give us the azimuth and elevation angles for a source located at a specific point in space.\n",
    "With these angles, we can then use the same function as in the previous part to auralize the source.\n",
    "\n",
    "Note that we are using OpenGL (Open Graphics Library) coordinates, which is a right-handed, y-up coordinate system.\n",
    "As such, the z-axis is pointing out of the screen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# no rotation of the receiver\n",
    "rotation = Rotation.from_euler(\"zyx\", [0, 0, 0], degrees=True)\n",
    "\n",
    "# calculate the azimuth and elevation angles relative to the receiver\n",
    "_, azimuth, elevation = hf.get_3D_metrics([0, 0, 0], rotation, [2, 0, -2])\n",
    "\n",
    "binaural_guitar = hf.convolve_with_hrir(\n",
    "    audio_data_guitar,\n",
    "    gain=0,\n",
    "    azimuth=azimuth,\n",
    "    elevation=elevation,\n",
    "    source_coordinates=source_coordinates,\n",
    "    hrir_data=data_ir,\n",
    ")\n",
    "\n",
    "hf.play_audio(binaural_guitar)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next let's look at this in an interactive setting. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.interactive_demo_source_pos(audio_data_guitar, source_coordinates, data_ir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That didn't work quite so well, did it?\n",
    "When our source is very far away, it sounds the same compared to when it is very close.\n",
    "\n",
    "To fix this, we have to consider the distance between the source and the receiver.\n",
    "The helper function also returns this as the first value.\n",
    "Again, this can be calculated using simple 3D math.\n",
    "\n",
    "With the distance, we have to consider two different propagation phenomena.\n",
    "\n",
    "1. Since the speed of sound is finite, it takes time for the sound to arrive at the receiver.\n",
    "2. With increasing distance, the sound will get quieter (due to the increase in the surface area of a sphere).\n",
    "   This is called 1/r or distance law.\n",
    "\n",
    "These two effects can be interpreted as a filter that can be applied to the audio signal.\n",
    "With the help of `create_transmission_filter` we can generate this filter.\n",
    "\n",
    "Let us look at the filter for different source positions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import IntSlider, interact, fixed\n",
    "\n",
    "\n",
    "def calculate_transmission_filter(source_x, source_z):\n",
    "    # create a rotation with no rotation\n",
    "    rotation = Rotation.from_euler(\"zyx\", [0, 0, 0], degrees=True)\n",
    "\n",
    "    # calculate the azimuth and elevation angles relative to the receiver\n",
    "    # in addition also get the distance\n",
    "    distance, azimuth, elevation = hf.get_3D_metrics(\n",
    "        [0, 0, 0], rotation, [source_x, 0, -source_z]\n",
    "    )\n",
    "\n",
    "    # this is for safy reasons, so that we do not divide by zero.\n",
    "    # we subtract 1m to account for the distance from which the HRIRs were measured.\n",
    "    distance = np.max((0.1, distance - 1.0))\n",
    "\n",
    "    speed_of_sound = 343  # [m/s]\n",
    "\n",
    "    # delay due to finite speed of sound, ref 1.\n",
    "    delay_seconds = distance / speed_of_sound\n",
    "\n",
    "    # 1/r law or distance law, ref 2.\n",
    "    distance_attenuation = 1 / distance\n",
    "\n",
    "    # get the hrir for the incident angles, similar to part 1.\n",
    "    index = hf.get_index_for_direction(source_coordinates, azimuth, elevation)\n",
    "    hrir = data_ir[index]\n",
    "\n",
    "    # interpret the propagation phenomena as a filter\n",
    "    transmission_filter = hf.create_transmission_filter(\n",
    "        distance_attenuation, delay_seconds, hrir\n",
    "    )\n",
    "\n",
    "    return transmission_filter\n",
    "\n",
    "\n",
    "def plot_transmission_filter(source_x, source_z, ax):\n",
    "    transmission_filter = calculate_transmission_filter(source_x, source_z)\n",
    "\n",
    "    ax[0].cla()\n",
    "    ax[1].cla()\n",
    "    pf.plot.time_freq(transmission_filter, label=[\"Left ear\", \"Right ear\"], ax=ax)\n",
    "    ax[0].legend()\n",
    "    ax[1].legend()\n",
    "\n",
    "\n",
    "slider_source_x = IntSlider(\n",
    "    value=0,\n",
    "    min=-20,\n",
    "    max=20,\n",
    "    step=1,\n",
    "    description=\"Source X [m]\",\n",
    "    continuous_update=False,\n",
    ")\n",
    "slider_source_z = IntSlider(\n",
    "    value=5, min=1, max=50, step=1, description=\"Source Z [m]\", continuous_update=False\n",
    ")\n",
    "\n",
    "fig, ax = fig, ax = plt.subplots(1, 2, figsize=(15, 4))\n",
    "\n",
    "interactive_panel = interact(\n",
    "    plot_transmission_filter,\n",
    "    source_x=slider_source_x,\n",
    "    source_z=slider_source_z,\n",
    "    ax=fixed(ax),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can guess what comes next: Listen!\n",
    "\n",
    "Here the horns are auralized at a fixed position as well in order to have a reference to compare the guitar signal to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def auralize_transmission_filter(\n",
    "    source_1_x, source_1_z, source_2_x, source_2_z, audio_data_1, audio_data_2\n",
    "):\n",
    "    transmission_filter_1 = calculate_transmission_filter(source_1_x, source_1_z)\n",
    "    transmission_filter_2 = calculate_transmission_filter(source_2_x, source_2_z)\n",
    "\n",
    "    auralization = hf.mix_audio(\n",
    "        pf.dsp.convolve(audio_data_1, transmission_filter_1),\n",
    "        pf.dsp.convolve(audio_data_2, transmission_filter_2),\n",
    "    )\n",
    "\n",
    "    hf.play_audio(auralization)\n",
    "\n",
    "\n",
    "slider_source_x = IntSlider(\n",
    "    value=0,\n",
    "    min=-20,\n",
    "    max=20,\n",
    "    step=1,\n",
    "    description=\"Source X [m]\",\n",
    "    continuous_update=False,\n",
    ")\n",
    "slider_source_z = IntSlider(\n",
    "    value=5, min=1, max=50, step=1, description=\"Source Z [m]\", continuous_update=False\n",
    ")\n",
    "\n",
    "interactive_panel = interact(\n",
    "    auralize_transmission_filter,\n",
    "    source_1_x=slider_source_x,\n",
    "    source_1_z=slider_source_z,\n",
    "    source_2_x=fixed(2),\n",
    "    source_2_z=fixed(0),\n",
    "    audio_data_1=fixed(audio_data_guitar),\n",
    "    audio_data_2=fixed(audio_data_horns),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While listening to this you will mainly notice the difference in level.\n",
    "The time shift is not as apparent in this example.\n",
    "However, when the distance to the source increases, like in an example of thunder and lightning, this effect gets more prominent.\n",
    "With the same example, one can also notice the effect of medium absorption which we will not consider here.\n",
    "Just know, that with increasing distance, high frequencies get attenuated more compared to low frequencies.\n",
    "\n",
    "We are still missing one piece of the puzzle: Source directivities!\n",
    "As you can imagine, a person radiates less sound towards the back while speaking.\n",
    "This effect can be measured and combined into a directivity dataset similar to the HRTF dataset.\n",
    "\n",
    "First, we will plot the filters for the horizontal plane:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load the dataset of a singer\n",
    "singer_mag, singer_source_coordinates, singer_receiver_coordinates = pf.io.read_sofa(\n",
    "    \"../data/directivity/Singer_2011_FWE_TLE_norm.sofa\"\n",
    ")\n",
    "\n",
    "# we have to do some housekeeping so that we can use the dataset here.\n",
    "singer_receiver_coordinates = singer_receiver_coordinates.cartesian.squeeze()\n",
    "singer_receiver_coordinates = pf.Coordinates.from_cartesian(\n",
    "    singer_receiver_coordinates[:, 0],\n",
    "    singer_receiver_coordinates[:, 1],\n",
    "    singer_receiver_coordinates[:, 2],\n",
    ")\n",
    "\n",
    "singer_ir = pf.Signal(pf.dsp.fft.irfft(singer_mag.freq, 60, 44100, \"none\"), 44100)\n",
    "singer_ir = pf.dsp.minimum_phase(singer_ir)\n",
    "\n",
    "slider_source_rotation = IntSlider(\n",
    "    value=0,\n",
    "    min=-180,\n",
    "    max=180,\n",
    "    step=5,\n",
    "    description=\"Source rotation [deg]\",\n",
    "    continuous_update=False,\n",
    ")\n",
    "\n",
    "fig, ax = fig, ax = plt.subplots(1, 2, figsize=(14, 4))\n",
    "\n",
    "\n",
    "def plot_directivity(rotation, ax):\n",
    "    ax[0].cla()\n",
    "    ax[1].cla()\n",
    "    index = hf.get_index_for_direction(singer_receiver_coordinates, rotation, 0)\n",
    "    pf.plot.time_freq(singer_ir[index], ax=ax)\n",
    "\n",
    "\n",
    "interactive_panel = interact(\n",
    "    plot_directivity,\n",
    "    rotation=slider_source_rotation,\n",
    "    ax=fixed(ax),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we will listen to it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_transmission_filter_with_directivity(source_rotation):\n",
    "    # first relative to receiver\n",
    "    rotation = Rotation.from_euler(\"zyx\", [0, 0, 0], degrees=True)\n",
    "    distance, azimuth, elevation = hf.get_3D_metrics([0, 0, 0], rotation, [0, 0, -2])\n",
    "\n",
    "    distance = np.max((0.1, distance - 2.0))\n",
    "\n",
    "    speed_of_sound = 343  # [m/s]\n",
    "\n",
    "    delay_seconds = distance / speed_of_sound\n",
    "\n",
    "    distance_attenuation = 1 / distance\n",
    "\n",
    "    index = hf.get_index_for_direction(source_coordinates, azimuth, elevation)\n",
    "    hrir = data_ir[index]\n",
    "\n",
    "    # then relative to source\n",
    "    rotation = Rotation.from_euler(\"zyx\", [0, source_rotation, 0], degrees=True)\n",
    "    _, azimuth, elevation = hf.get_3D_metrics([0, 0, 0], rotation, [0, 0, -2])\n",
    "\n",
    "    index = hf.get_index_for_direction(singer_receiver_coordinates, azimuth, elevation)\n",
    "    directivity = singer_ir[index]\n",
    "\n",
    "    transmission_filter = hf.create_transmission_filter(\n",
    "        distance_attenuation, delay_seconds, hrir, directivity\n",
    "    )\n",
    "\n",
    "    return transmission_filter\n",
    "\n",
    "\n",
    "def auralize_transmission_filter_with_directivity(source_rotation, audio_data):\n",
    "    transmission_filter = calculate_transmission_filter_with_directivity(\n",
    "        source_rotation\n",
    "    )\n",
    "\n",
    "    auralization = pf.dsp.convolve(audio_data, transmission_filter)\n",
    "\n",
    "    hf.play_audio(auralization)\n",
    "\n",
    "\n",
    "slider_source_rotation = IntSlider(\n",
    "    value=0,\n",
    "    min=-180,\n",
    "    max=180,\n",
    "    step=5,\n",
    "    description=\"Source rotation [deg]\",\n",
    "    continuous_update=False,\n",
    ")\n",
    "\n",
    "interactive_panel = interact(\n",
    "    auralize_transmission_filter_with_directivity,\n",
    "    source_rotation=slider_source_rotation,\n",
    "    audio_data=fixed(audio_data_guitar),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can put it all together to create a full free-field auralization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_transmission_filter_full(\n",
    "    receiver_pos, receiver_rotation, source_pos, source_rot\n",
    "):\n",
    "    if (np.array(receiver_pos) == np.array(source_pos)).all():\n",
    "        raise ValueError(\"Source and receiver are in the same position.\")\n",
    "\n",
    "    # first relative to receiver\n",
    "    distance, azimuth, elevation = hf.get_3D_metrics(\n",
    "        receiver_pos, receiver_rotation, source_pos\n",
    "    )\n",
    "\n",
    "    distance = np.max((0.1, distance - 2.0))\n",
    "\n",
    "    speed_of_sound = 343  # [m/s]\n",
    "\n",
    "    delay_seconds = distance / speed_of_sound\n",
    "\n",
    "    distance_attenuation = 1 / distance\n",
    "\n",
    "    index = hf.get_index_for_direction(source_coordinates, azimuth, elevation)\n",
    "    hrir = data_ir[index]\n",
    "\n",
    "    # then relative to source\n",
    "    _, azimuth, elevation = hf.get_3D_metrics(source_pos, source_rot, receiver_pos)\n",
    "\n",
    "    index = hf.get_index_for_direction(singer_receiver_coordinates, azimuth, elevation)\n",
    "    directivity = singer_ir[index]\n",
    "\n",
    "    transmission_filter = hf.create_transmission_filter(\n",
    "        distance_attenuation, delay_seconds, hrir, directivity\n",
    "    )\n",
    "\n",
    "    return transmission_filter\n",
    "\n",
    "\n",
    "def auralize_transmission_filter_with_directivity(\n",
    "    receiver_x,\n",
    "    receiver_y,\n",
    "    receiver_z,\n",
    "    receiver_rot,\n",
    "    source_x,\n",
    "    source_y,\n",
    "    source_z,\n",
    "    source_rot,\n",
    "    audio_data,\n",
    "):\n",
    "    source_rot = Rotation.from_euler(\"zyx\", [0, source_rot, 0], degrees=True)\n",
    "    receiver_rot = Rotation.from_euler(\n",
    "        \"zyx\", [0, receiver_rot, 0], degrees=True\n",
    "    )\n",
    "    transmission_filter = calculate_transmission_filter_full(\n",
    "        [receiver_x, receiver_y, receiver_z],\n",
    "        receiver_rot,\n",
    "        [source_x, source_y, source_z],\n",
    "        source_rot,\n",
    "    )\n",
    "\n",
    "    auralization = pf.dsp.convolve(audio_data, transmission_filter)\n",
    "\n",
    "    hf.play_audio(auralization)\n",
    "\n",
    "\n",
    "hf.interactive_demo_FF_full(\n",
    "    audio_data_guitar, auralize_transmission_filter_with_directivity\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now go to the [next notebook](../03_Virtual_Acoustics/Virtual_Acoustics.ipynb)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
