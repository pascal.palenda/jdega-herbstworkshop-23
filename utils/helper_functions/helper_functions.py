import pyfar as pf
import numpy as np
from IPython.display import Audio
from IPython.core.display import display
from ipywidgets import (
    GridspecLayout,
    Button,
    Layout,
    IntSlider,
    fixed,
    interactive_output,
)
from scipy.spatial.transform import Rotation
from pathlib import Path
import os


def get_index_for_direction(source_coordinates, azimuth, elevation):
    coordinates = pf.Coordinates.from_spherical_elevation(
        azimuth * np.pi / 180, elevation * np.pi / 180, 2
    )

    index, _ = source_coordinates.find_nearest(coordinates)

    return index


def convolve_with_hrir(
    audio_data, gain, azimuth, elevation, source_coordinates, hrir_data
):
    hrir_index = get_index_for_direction(source_coordinates, azimuth, elevation)

    hrir = hrir_data[hrir_index]

    output_audio = 10 ** (gain / 20) * pf.dsp.convolve(audio_data, hrir)

    return output_audio


def mix_audio(*audio_data):
    summed = pf.classes.audio.add(audio_data, domain="time")

    return pf.dsp.normalize(summed)


def play_audio(audio):
    display(Audio(audio.time, rate=audio.sampling_rate, autoplay=True))


def interactive_demo(
    audio_data_horns,
    audio_data_guitar,
    source_coordinates,
    hrir_data,
):
    def create_expanded_button(description):
        return Button(
            description=description,
            button_style="success",
            layout=Layout(height="auto", width="auto"),
        )

    slider_azimuth_git = IntSlider(
        value=90,
        min=-180,
        max=180,
        step=5,
        description="Azimuth [deg]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    slider_elevation_git = IntSlider(
        value=0,
        min=-90,
        max=90,
        step=5,
        description="Elevation [deg]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    slider_gain_git = IntSlider(
        value=0,
        min=-50,
        max=0,
        step=1,
        description="Gain [dB]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )

    slider_azimuth_horns = IntSlider(
        value=0,
        min=-180,
        max=180,
        step=5,
        description="Azimuth [deg]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    slider_elevation_horns = IntSlider(
        value=0,
        min=-90,
        max=90,
        step=5,
        description="Elevation [deg]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    slider_gain_horns = IntSlider(
        value=0,
        min=-50,
        max=0,
        step=1,
        description="Gain [dB]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )

    grid = GridspecLayout(4, 2, height="200px")
    grid[0, 0] = create_expanded_button("Horns")
    grid[1, 0] = slider_azimuth_horns
    grid[2, 0] = slider_elevation_horns
    grid[3, 0] = slider_gain_horns
    grid[0, 1] = create_expanded_button("Guitar")
    grid[1, 1] = slider_azimuth_git
    grid[2, 1] = slider_elevation_git
    grid[3, 1] = slider_gain_git

    def gen_binaural_synthesis(
        audio_data_horns,
        gain_horns,
        azimuth_horns,
        elevation_horns,
        audio_data_guitar,
        gain_guitar,
        azimuth_guitar,
        elevation_guitar,
        source_coordinates,
        hrir_data,
    ):
        bin_horn = convolve_with_hrir(
            audio_data_horns,
            gain_horns,
            azimuth_horns,
            elevation_horns,
            source_coordinates,
            hrir_data,
        )
        bin_git = convolve_with_hrir(
            audio_data_guitar,
            gain_guitar,
            azimuth_guitar,
            elevation_guitar,
            source_coordinates,
            hrir_data,
        )

        play_audio(mix_audio(bin_horn, bin_git))

    out = interactive_output(
        gen_binaural_synthesis,
        {
            "audio_data_horns": fixed(audio_data_horns),
            "gain_horns": slider_gain_horns,
            "azimuth_horns": slider_azimuth_horns,
            "elevation_horns": slider_elevation_horns,
            "audio_data_guitar": fixed(audio_data_guitar),
            "gain_guitar": slider_gain_git,
            "azimuth_guitar": slider_azimuth_git,
            "elevation_guitar": slider_elevation_git,
            "source_coordinates": fixed(source_coordinates),
            "hrir_data": fixed(hrir_data),
        },
    )

    display(grid, out)


def rotation_to_view_up_vectors(rotation_matrix):
    # Extract the right, up, and view vectors from the rotation matrix
    up_vector = rotation_matrix[:, 1]
    view_vector = -rotation_matrix[:, 2]

    # Optionally normalize the vectors if needed
    up_vector /= np.linalg.norm(up_vector)
    view_vector /= np.linalg.norm(view_vector)

    return view_vector, up_vector


def get_3D_metrics(source_position, source_orientation, target_position):
    if not isinstance(source_position, np.ndarray):
        source_position = np.array(source_position)
    if not isinstance(target_position, np.ndarray):
        target_position = np.array(target_position)
    if not isinstance(source_orientation, Rotation):
        TypeError("source_orientation must be of type scipy.spatial.transform.Rotation")

    direction = target_position - source_position
    distance = np.linalg.norm(direction)

    view, up = rotation_to_view_up_vectors(source_orientation.as_matrix())

    x_dash = np.cross(view, up)
    y_dash = up
    z_dash = -view

    w_x_local = np.dot(direction, x_dash)
    w_y_local = np.dot(direction, y_dash)
    w_z_local = np.dot(direction, z_dash)

    azimuth_deg = np.degrees(np.arctan2(-w_x_local, -w_z_local))

    w_y_local_normalized = w_y_local / np.linalg.norm([w_x_local, w_y_local, w_z_local])
    elevation_deg = np.degrees(np.arcsin(w_y_local_normalized))

    return distance, azimuth_deg, elevation_deg


def interactive_demo_source_pos(
    audio_data,
    source_coordinates,
    hrir_data,
):
    def create_expanded_button(description):
        return Button(
            description=description,
            button_style="success",
            layout=Layout(height="auto", width="auto"),
        )

    source_x = IntSlider(
        value=0,
        min=-10,
        max=10,
        step=1,
        description="Source X [m]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    source_y = IntSlider(
        value=0,
        min=-10,
        max=10,
        step=1,
        description="Source Y [m]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )
    source_z = IntSlider(
        value=2,
        min=-10,
        max=10,
        step=1,
        description="Source Z [m]",
        continuous_update=False,
        layout=Layout(height="auto", width="auto"),
    )

    grid = GridspecLayout(4, 1, height="200px")
    grid[0, 0] = create_expanded_button("Source position")
    grid[1, 0] = source_x
    grid[2, 0] = source_y
    grid[3, 0] = source_z

    def gen_binaural_synthesis(
        audio_data,
        source_x,
        source_y,
        source_z,
        source_coordinates,
        hrir_data,
    ):
        rotation = Rotation.from_euler("zyx", [0, 0, 0], degrees=True)
        _, azimuth, elevation = get_3D_metrics(
            [0, 0, 0], rotation, [source_x, source_y, source_z]
        )

        binaural_signal = convolve_with_hrir(
            audio_data,
            0,
            azimuth,
            elevation,
            source_coordinates,
            hrir_data,
        )

        play_audio(binaural_signal)

    out = interactive_output(
        gen_binaural_synthesis,
        {
            "audio_data": fixed(audio_data),
            "source_x": source_x,
            "source_y": source_y,
            "source_z": source_z,
            "source_coordinates": fixed(source_coordinates),
            "hrir_data": fixed(hrir_data),
        },
    )

    display(grid, out)


def create_transmission_filter(
    attenuation, delay_seconds, receiver_ir, source_ir=pf.signals.impulse(64)
):
    sample_rate = 44100
    delay_samples = int(np.round(delay_seconds * sample_rate))
    impulse = pf.signals.impulse(2**13, delay=delay_samples)

    impulse *= attenuation

    return pf.dsp.convolve(pf.dsp.convolve(impulse, source_ir), receiver_ir)


def interactive_demo_FF_full(
    audio_data,
    func,
):
    def create_expanded_button(description):
        return Button(
            description=description,
            button_style="success",
            layout=Layout(height="auto", width="auto"),
        )

    data = {}
    grid = GridspecLayout(5, 2, height="200px")

    for idx, obj in enumerate(["receiver", "source"]):
        data["{}_x".format(obj)] = IntSlider(
            value=0 if obj == "receiver" else 5,
            min=-10,
            max=10,
            step=1,
            description="{} X [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_y".format(obj)] = IntSlider(
            value=0,
            min=-10,
            max=10,
            step=1,
            description="{} Y [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_z".format(obj)] = IntSlider(
            value=0,
            min=-10,
            max=10,
            step=1,
            description="{} Z [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_rot".format(obj)] = IntSlider(
            value=0,
            min=-180,
            max=180,
            step=5,
            description="{} Yaw [deg]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )

        grid[0, idx] = create_expanded_button("{} position".format(obj))
        grid[1, idx] = data["{}_x".format(obj)]
        grid[2, idx] = data["{}_y".format(obj)]
        grid[3, idx] = data["{}_z".format(obj)]
        grid[4, idx] = data["{}_rot".format(obj)]

    data["audio_data"] = fixed(audio_data)

    out = interactive_output(func, data)

    display(grid, out)


def start_va_server(config: Path | None = None):
    script_dir = Path(__file__).parent.resolve()

    download_dir = script_dir / ".." / ".." / "download"

    va_server_list = list(download_dir.glob("VA*/bin/VAServer.exe"))

    if not va_server_list:
        raise LookupError("No VAServer found!")

    va_server = va_server_list[0]

    va_start_dir = va_server.parent.parent

    if not config:
        va_conf_list = list(download_dir.glob("VA*/conf/VACore.ini"))
        if not va_conf_list:
            raise LookupError("No config file found!")
        config = va_conf_list[0]

    # subprocess.Popen(["start", "cmd", "/K", "dir"])
    os.system(
        'start cmd /K "cd {} && {} localhost:12340 {}"'.format(
            va_start_dir, va_server, config
        )
    )


def interactive_demo_VA(
    func,
):
    def create_expanded_button(description):
        return Button(
            description=description,
            button_style="success",
            layout=Layout(height="auto", width="auto"),
        )

    data = {}
    grid = GridspecLayout(5, 2, height="200px")

    for idx, obj in enumerate(["receiver", "source"]):
        data["{}_x".format(obj)] = IntSlider(
            value=0 if obj == "receiver" else 5,
            min=-10,
            max=10,
            step=1,
            description="{} X [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_y".format(obj)] = IntSlider(
            value=0,
            min=-10,
            max=10,
            step=1,
            description="{} Y [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_z".format(obj)] = IntSlider(
            value=0,
            min=-10,
            max=10,
            step=1,
            description="{} Z [m]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )
        data["{}_rot".format(obj)] = IntSlider(
            value=0,
            min=-180,
            max=180,
            step=5,
            description="{} Yaw [deg]".format(obj),
            continuous_update=False,
            layout=Layout(height="auto", width="auto"),
        )

        grid[0, idx] = create_expanded_button("{} position".format(obj))
        grid[1, idx] = data["{}_x".format(obj)]
        grid[2, idx] = data["{}_y".format(obj)]
        grid[3, idx] = data["{}_z".format(obj)]
        grid[4, idx] = data["{}_rot".format(obj)]

    out = interactive_output(func, data)

    display(grid, out)
