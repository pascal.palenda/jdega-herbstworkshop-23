{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Binaural synthesis\n",
    "\n",
    "First, we will take a look at the basics of binaural synthesis.\n",
    "\n",
    "Humans can localize sound events about their perceived distance as well as their angular position in space.\n",
    "Sound waves are altered due to reflections, diffraction, and resonances caused by the presence of a human body, that is head, shoulders, torso, as well as the fine structure of the ear formed by pinna, cavum conchae, etc.\n",
    "All these effects, which in its assembly are evaluated by the human brain to localize a source or to get other spatial information, are integrated into binaural signals.\n",
    "If the binaural signal is reproduced perfectly at the eardrums (the human microphones), there is no chance to distinguish the virtual source or environment from the real sound field.\n",
    "With binaural synthesis, a filtering approach with special filters, an acoustic sound source represented by a mono-signal can virtually be placed at arbitrary positions in space.\n",
    "\n",
    "## HRTF dataset\n",
    "\n",
    "A valid way to describe all linear sound transformations caused by torso, head and pinna is the use of so-called “head-related transfer functions” (HRTFs).\n",
    "For each direction of sound incidence from a sound source to a human receiver there exist two transfer functions (one for the left and one for the right ear), which are combined into a two-channel HRTF in the frequency domain.\n",
    "The combination of all directions into a single database is commonly called an HRTF dataset.\n",
    "The time domain version of HRTFs are also known as \"head-related impulse responses\" (HRIRs).\n",
    "\n",
    "Let's take a look at this!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget\n",
    "# import the required packages\n",
    "import pyfar as pf\n",
    "import sofar as sf\n",
    "import helper_functions.helper_functions as hf\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# load the HRIR dataset\n",
    "data_ir, source_coordinates, receiver_coordinates = pf.io.read_sofa(\n",
    "    \"../data/hrir/ITA_Artificial_Head_5x5_44100Hz.sofa\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we plot all the source locations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = hf.get_index_for_direction(source_coordinates, 90, 0)\n",
    "source_coordinates.show(index)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of the dots represents the source location of one two-channel HRTF.\n",
    "You might notice one red dot.\n",
    "This is the HRTF that we selected with the helper function `get_index_for_direction`.\n",
    "It contains the HRTF for a source coming from the left.\n",
    "\n",
    "Let us plot this HRTF/HRIR."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "ax = pf.plot.time_freq(data_ir[index],label=[\"Left ear\", \"Right ear\"])\n",
    "ax[0].legend();\n",
    "ax[1].legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see in the time domain, that the impulse for the left ear is louder and arrives earlier compared to the right ear.\n",
    "These effects are called interaural time and level difference, ITD and ILD for short.\n",
    "\n",
    "In the frequency domain, we can see the ILD again.\n",
    "We can also see, that there are differences in the fine structure of the transfer functions between the left and right ear.\n",
    "\n",
    "These differences are used by the human brain to localize sound."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Auralization\n",
    "\n",
    "In order to auralize a sound source from a certain direction we first need a source signal.\n",
    "In the following sections, we will load two different audio signals.\n",
    "Note, that these are mono audio files with just one channel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "audio_data_guitar = pf.io.read_audio(\"../data/audio/guitar.wav\")\n",
    "audio_data_horns = pf.io.read_audio(\"../data/audio/horns.wav\")\n",
    "\n",
    "hf.play_audio(audio_data_guitar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.play_audio(audio_data_horns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To apply an HRIR we have to convolve the mono audio signal with the two impulse responses for the left and right ear.\n",
    "If we then use headphones to listen to the two-channel output signal, we should perceive the signal as coming from the chosen direction.\n",
    "\n",
    "Let's listen to this!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binaural_guitar = hf.convolve_with_hrir(\n",
    "    audio_data_guitar,\n",
    "    gain=0,\n",
    "    azimuth=90,\n",
    "    elevation=0,\n",
    "    source_coordinates=source_coordinates,\n",
    "    hrir_data=data_ir,\n",
    ")\n",
    "\n",
    "hf.play_audio(binaural_guitar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binaural_horns = hf.convolve_with_hrir(\n",
    "    audio_data_horns,\n",
    "    gain=0,\n",
    "    azimuth=-90,\n",
    "    elevation=0,\n",
    "    source_coordinates=source_coordinates,\n",
    "    hrir_data=data_ir,\n",
    ")\n",
    "\n",
    "hf.play_audio(binaural_horns)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will combine the two signals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "binaural_synthesis = hf.mix_audio(binaural_guitar, binaural_horns)\n",
    "\n",
    "hf.play_audio(binaural_synthesis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the following section, we can interactively adapt the binaural synthesis.\n",
    "\n",
    "Keep in mind, that this uses an HRIR dataset from a dummy head.\n",
    "This means, that you might have localization problems, especially on the cones of confusion.\n",
    "This is due to the fact, that the dummy head is not shaped exactly like you.\n",
    "As a result, the fine structure of the HRTF is not what your brain expects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hf.interactive_demo(audio_data_horns, audio_data_guitar, source_coordinates, data_ir)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now go to the [next notebook](../02_Free_Field_Synthesis/Free_Field_Synthesis.ipynb)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
