@REM @echo off

python --version

if ERRORLEVEL 1 (
    echo "python not installed, please install it"
    exit
)

IF NOT EXIST ".env" (
    python -m venv .env
)

call .env/Scripts/activate.bat

pip install -r requirements.txt

pip install -e utils

python get_va.py
